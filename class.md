### classes[4] = "Word Processing Power Usage"

#### Perspetivas em Bioinformática 2024-2025

![Logo EST](assets/logo-ESTB.png)

Francisco Pina Martins

<p><img style="vertical-align:middle" src="../presentation_assets/mastodon-icon.svg" alt="Mastodon icon" width="40px", height="40px"> <a href="https://scholar.social/@FPinaMartins">@FPinaMartins@scholar.social</a></p>

---

### Word processing

![Typewriter](assets/old-black-typewriter.png)

---

### Types of word processing

<div class="fragment" style="float: left;width:50%">

* What You See Is What You Get
  * &shy;<!-- .element: class="fragment" -->Typed text and objects appear exactly as they are placed
  * &shy;<!-- .element: class="fragment" -->Typesetting and content are managed simultaneously


</div>

<div class="fragment" style="float: right;width:50%">

* What You See Is What You Mean
  * &shy;<!-- .element: class="fragment" -->Typed text and objects reflect a "meaning", not a final form
  * &shy;<!-- .element: class="fragment" -->Typesetting and content are separate
    * &shy;<!-- .element: class="fragment" -->Document needs to be interpreted into it's "final form"

</div>

---

### WYSIWYM

&shy;<!-- .element: class="fragment" -->![What you see is what you mean logos](assets/wysiwym_logos.png)

---

### WYSIWYG


<div class="r-stack">
  <img class="fragment" src="assets/icons.png">
  <img class="fragment" src="assets/icons_highlight.png">
</div>

---

### Direct formatting

* &shy;<!-- .element: class="fragment" -->Simple to use
* &shy;<!-- .element: class="fragment" -->Instant result
* &shy;<!-- .element: class="fragment" -->Define how text *looks like*

&shy;<!-- .element: class="fragment" -->![Direct formatting](assets/direct.png)

* &shy;<!-- .element: class="fragment" -->Manual
* &shy;<!-- .element: class="fragment" -->Error prone

---

### Speeding up direct formatting

* &shy;<!-- .element: class="fragment" -->Ctrl + b (**Bold**)
* &shy;<!-- .element: class="fragment" -->Ctrl + i (*Italics*)
* &shy;<!-- .element: class="fragment" -->Ctrl + u (___Underline___)
* &shy;<!-- .element: class="fragment" -->Ctrl + ] or [ (increase or decrease font size)
* &shy;<!-- .element: class="fragment" -->Ctrl + z (undo)
* &shy;<!-- .element: class="fragment" -->[etc...](https://www.makeuseof.com/libreoffice-writer-keyboard-shortcuts/amp/)

&shy;<!-- .element: class="fragment" -->![60% keyboard](assets/keyboard.png)

---

### Automation

* &shy;<!-- .element: class="fragment" -->Formatting can be automated using *Styles*
  * &shy;<!-- .element: class="fragment" --><font color="forestgreen">**Page**</font>
  * &shy;<!-- .element: class="fragment" --><font color="forestgreen">**Paragraph**</font>
  * &shy;<!-- .element: class="fragment" --><font color="red">Character</font>
  * &shy;<!-- .element: class="fragment" --><font color="red">Table</font>
  * &shy;<!-- .element: class="fragment" --><font color="red">List</font>
  * &shy;<!-- .element: class="fragment" --><font color="red">Frame</font>
* &shy;<!-- .element: class="fragment" -->Define what text *is*

<div class="fragment" style="float: right">

![Printing Press](assets/press.jpg)

</div>

---

### Page Styles

* &shy;<!-- .element: class="fragment" -->(Uniquely) Named
* &shy;<!-- .element: class="fragment" -->Define page settings:
  * &shy;<!-- .element: class="fragment" -->Margins
  * &shy;<!-- .element: class="fragment" -->Header & footer
  * &shy;<!-- .element: class="fragment" -->Size and orientation
  * &shy;<!-- .element: class="fragment" -->Layout
  * &shy;<!-- .element: class="fragment" -->etc...
* &shy;<!-- .element: class="fragment" -->Use **breaks** to set

<div class="fragment" style="float: right">

![Page breaks](assets/breaks.png)

</div>

&shy;<!-- .element: class="fragment" -->[Learn more](https://books.libreoffice.org/en/WG76/WG7605-FormattingPagesBasics.html#toc6)

---

### Paragraph styles

* &shy;<!-- .element: class="fragment" -->(Uniquely) Named
* &shy;<!-- .element: class="fragment" -->Define a set of characteristics such as:
  * &shy;<!-- .element: class="fragment" -->Font type and size
  * &shy;<!-- .element: class="fragment" -->Indentation
  * &shy;<!-- .element: class="fragment" -->Alignment
* &shy;<!-- .element: class="fragment" -->Hierarchical
  * &shy;<!-- .element: class="fragment" -->Inherit characteristics from the "parent" style
* &shy;<!-- .element: class="fragment" -->Styles are then applied to text parts

<div class="fragment" style="float: right">

![Live](assets/now-live-icon.png)

</div>

---

### Table of contents

* &shy;<!-- .element: class="fragment" -->Using styles allows you to automate TOC creation
  * &shy;<!-- .element: class="fragment" -->"Insert" -> "Table of contents and index"
* &shy;<!-- .element: class="fragment" -->Controlled under "Tools" -> "Chapter numbering..." ("Heading numbering..." on newer LO versions)

&shy;<!-- .element: class="fragment" -->![TOC handling](assets/TOC_styles.png)

&shy;<!-- .element: class="fragment" -->[Learn more](https://books.libreoffice.org/en/WG76/WG7608-IntroStyles.html#bkmRefHeadingToc145391778402156)

---

### Figures & Tables

<div class="fragment" style="float: left;width:50%">

* &shy;<!-- .element: class="fragment" -->**Must** be referenced in the text
  * &shy;<!-- .element: class="fragment" -->**Before** each is presented
* &shy;<!-- .element: class="fragment" -->Figure captions are typically placed **at the bottom** of the frame
* &shy;<!-- .element: class="fragment" -->Table captions are typically placed **at the top** of the frame
* &shy;<!-- .element: class="fragment" -->Use *Captions* to automate counting
* &shy;<!-- .element: class="fragment" -->Use [Anchors](https://books.libreoffice.org/en/WG76/WG7611-ImagesAndGraphics.html#toc20) to get fine grained control on image placement

</div>

<div class="fragment" style="float: right">

![Caption dialogue](assets/caption.png)

</div>

</br>
</br>
</br>
</br>
</br>
</br>
</br>
</br>
</br>
</br>
</br>

&shy;<!-- .element: class="fragment" -->[Learn more](https://books.libreoffice.org/en/WG76/WG7611-ImagesAndGraphics.html#toc24)

---

### Fields

<div class="fragment" style="float: left;width:50%">

* Easy way to display non-static metadata
  * &shy;<!-- .element: class="fragment" -->Date
  * &shy;<!-- .element: class="fragment" -->Page number
  * &shy;<!-- .element: class="fragment" -->Title
* &shy;<!-- .element: class="fragment" -->Especially useful for templates and headers/footers

</div>

<div class="fragment" style="float: right;width:50%">

![Fields dialog](assets/fields.png)

</div>

</br>
</br>
</br>
</br>
</br>

&shy;<!-- .element: class="fragment" -->[Learn more](https://books.libreoffice.org/en/WG76/WG7617-Fields.html#toc7)

|||

### Cross references

<div class="fragment" style="float: left;width:40%">

</br>

* Are a special type of "field"
* &shy;<!-- .element: class="fragment" -->Can link to any uniquely identified text element
  * &shy;<!-- .element: class="fragment" -->Headings
  * &shy;<!-- .element: class="fragment" -->Figures
  * &shy;<!-- .element: class="fragment" -->Pages
  * &shy;<!-- .element: class="fragment" -->etc..

</div>

<div class="fragment" style="float: right;width:60%">

![Cross reference dialogue](assets/cross-references.png)

</div>

</br>
</br>
</br>
</br>
</br>
</br>
</br>
</br>
</br>
</br>

&shy;<!-- .element: class="fragment" -->[Learn more](https://books.libreoffice.org/en/WG76/WG7617-Fields.html#toc12)

---

### What's in a cover?

* &shy;<!-- .element: class="fragment" -->Date
* &shy;<!-- .element: class="fragment" -->Title
* &shy;<!-- .element: class="fragment" -->Identification 
  * &shy;<!-- .element: class="fragment" -->Author(s)
  * &shy;<!-- .element: class="fragment" -->**Context**
* &shy;<!-- .element: class="fragment" -->School logo

&shy;<!-- .element: class="fragment" -->![ESTB-logo](assets/logo-ESTB.png)

|||

### Cover example

![Cover example](assets/cover.png)

---

### Templates

* &shy;<!-- .element: class="fragment" -->*Model* documents
* &shy;<!-- .element: class="fragment" -->Used to create other documents
* &shy;<!-- .element: class="fragment" -->Can contain text, images, styles, and even toolbars
* &shy;<!-- .element: class="fragment" -->Perfect to apply all you have previously learned

&shy;<!-- .element: class="fragment" -->![Car chassis](assets/chassis.png)

&shy;<!-- .element: class="fragment" -->[Learn more](https://books.libreoffice.org/en/WG76/WG7610-WorkingWithTemplates.html)

---

### Master Documents

<div class="fragment" style="float: left;width:50%">

* A *container* that joins multiple different documents
* &shy;<!-- .element: class="fragment" -->Unifies **styles**, **fields**, **TOCs**, etc...
* &shy;<!-- .element: class="fragment" -->Very useful to:
  * &shy;<!-- .element: class="fragment" -->Join various stand-alone documents
  * &shy;<!-- .element: class="fragment" -->Have several separate bibliographic entries
  * &shy;<!-- .element: class="fragment" -->Allow multiple people to work in different document sections

</div>

<div class="fragment" style="float: right;width:50%">

![Master document](assets/megazord.jpg)

</div>

&shy;<!-- .element: class="fragment" -->[Learn more](https://books.libreoffice.org/en/WG76/WG7616-MasterDocuments.html)

---

### Reference management

* &shy;<!-- .element: class="fragment" -->References are a **fundamental** part of any work
  * &shy;<!-- .element: class="fragment" -->*Stand on the shoulders of giants*
* &shy;<!-- .element: class="fragment" -->There are many ways to use references, but they all have in common:
  * &shy;<!-- .element: class="fragment" -->A reference list (full metadata)
  * &shy;<!-- .element: class="fragment" -->In-text citations (abbreviated form)
* &shy;<!-- .element: class="fragment" -->Style choice if up to authors (or journals)

&shy;<!-- .element: class="fragment" -->![References](assets/reference.png)

|||

### Automation

* &shy;<!-- .element: class="fragment" -->Manually writing references is **hard**
* &shy;<!-- .element: class="fragment" -->Software to the rescue
  * &shy;<!-- .element: class="fragment" -->Collects a reference database
  * &shy;<!-- .element: class="fragment" -->Inserts citations *in text*
  * &shy;<!-- .element: class="fragment" -->Creates reference list

&shy;<!-- .element: class="fragment" -->![Refrence manager logos](assets/ref_logos.png)

---

### Challange

* Create a report template
* Be sure to include
  * Cover
  * Page Styles
  * Paragraph Styles
  * Table of Contents

**This will save you hours of work in the long run**

---

### References

* [A guide to Libreoffice Writer 7.6](https://books.libreoffice.org/en/WG76/WG76.html)
* [Libreoffice writer keyboard shortcuts cheat sheet](https://www.makeuseof.com/libreoffice-writer-keyboard-shortcuts/amp/)
* [Zotero](https://zotero.org/)
* [Mendeley](https://www.mendeley.com/)
* [EndNote](https://endnote.com/)
